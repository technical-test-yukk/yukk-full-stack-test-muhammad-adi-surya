@extends('adminlte::page')

@section('title', 'User Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                    <span class="info-box-icon bg-primary elevation-1"><i class="fas fa-shopping-cart"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Order</span>
                        <span class="info-box-number">
                            {{ $orderCount }}
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <span class="info-box-icon bg-success elevation-1"><i class="fas fa-check-circle"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Completed</span>
                        <span class="info-box-number">
                            {{ $completedCount }}
                        </span>
                    </div>
                </div>
            </div>
            <div class="clearfix hidden-md-up"></div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-spinner"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">On Process</span>
                        <span class="info-box-number">
                            {{ $pendingCount }}
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-times-circle"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Rejected</span>
                        <span class="info-box-number">
                            {{ $rejectCount }}
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="card mt-3">
            <div class="card-header">
                <h3 class="card-title">10 Last Order</h3>
            </div>
            <div class="card-body">
                <table id="orders-table" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Order Number</th>
                        <th class="text-center">Type</th>
                        <th class="text-center">Status</th>
                        <th class="text-center">Amount</th>
                        <th class="text-center">Balance</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($latestOrders as $latestOrder)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $latestOrder->order_number }}</td>
                            <td class="text-center">
                                @if($latestOrder->type === 'top-up')
                                    <span class="badge bg-primary">{{ ucfirst($latestOrder->type) }}</span>
                                @elseif($latestOrder->type === 'transaction')
                                    <span class="badge bg-info">{{ ucfirst($latestOrder->type) }}</span>
                                @else
                                    <span class="badge bg-secondary">{{ ucfirst($latestOrder->type) }}</span>
                                @endif
                            </td>
                            <td class="text-center">
                                @if($latestOrder->status === 'pending')
                                    <span class="badge bg-warning">{{ ucfirst($latestOrder->status) }}</span>
                                @elseif($latestOrder->status === 'completed')
                                    <span class="badge bg-success">{{ ucfirst($latestOrder->status) }}</span>
                                @elseif($latestOrder->status === 'reject')
                                    <span class="badge bg-danger">{{ ucfirst($latestOrder->status) }}</span>
                                @else
                                    <span class="badge bg-secondary">{{ ucfirst($latestOrder->status) }}</span>
                                @endif
                            </td>
                            <td class="text-center"><b>{{ 'Rp ' . number_format($latestOrder->amount, 2, ',', '.') }}</b></td>
                            <td class="text-center">
                                @if($latestOrder->status === 'pending')
                                    <span class="text-warning"><b>- {{ 'Rp ' . number_format($latestOrder->amount, 2, ',', '.') }}</b></span>
                                @else
                                    @if($latestOrder->type === 'top-up')
                                        <span class="text-success"><b>+ {{ 'Rp ' . number_format($latestOrder->amount, 2, ',', '.') }}</b></span>
                                    @elseif($latestOrder->type === 'transaction')
                                        <span class="text-danger"><b>- {{ 'Rp ' . number_format($latestOrder->amount, 2, ',', '.') }}</b></span>
                                    @else
                                        <b>{{ 'Rp ' . number_format($latestOrder->amount, 2, ',', '.') }}</b>
                                    @endif
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop
