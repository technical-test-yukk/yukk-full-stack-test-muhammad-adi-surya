@extends('adminlte::page')

@section('title', 'Balance History')

@section('plugins.Datatables', true)

@section('content_header')
    <h1>Balance History</h1>
@stop

@section('content')
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Balance</h3>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="small-box bg-success">
                            <div class="inner">
                                <h3>Rp.{{ number_format($current_balance, 0, ',', '.') }}</h3>
                                <p>Current Balance</p>
                            </div>
                            <div class="icon mt-6 center">
                                <i class="fas fa-credit-card"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="small-box bg-primary">
                            <div class="inner">
                                <h3>Rp.{{ number_format($topup_total, 0, ',', '.') }}</h3>
                                <p>Total Top-Up</p>
                            </div>
                            <div class="icon mt-6 center">
                                <i class="fas fa-arrow-circle-down"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="small-box bg-info">
                            <div class="inner">
                                <h3>Rp.{{ number_format($transaction_total, 0, ',', '.') }}</h3>
                                <p>Total Transaction</p>
                            </div>
                            <div class="icon mt-6 center">
                                <i class="fas fa-arrow-circle-up"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <div class="row">
        <div class="col-md-3 mb-3">
            <label for="type_filter" class="form-label">Filter by Type:</label>
            <select id="type_filter" class="form-control select2">
                <option value="">All Types</option>
                <option value="top-up">Top-Up</option>
                <option value="transaction">Transaction</option>
            </select>
        </div>
    </div>
    <div class="card mt-3">
        <div class="card-body">
            <table id="orders-table" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Order Number</th>
                    <th class="text-center">Type</th>
                    <th class="text-center">Amount</th>
                    <th class="text-center">Balance</th>
                </tr>
                </thead>
                <tbody>
                @foreach($histories as $history)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $history->order_number }}</td>
                        <td class="text-center">
                            @if($history->type === 'top-up')
                                <span class="badge bg-primary">{{ ucfirst($history->type) }}</span>
                            @elseif($history->type === 'transaction')
                                <span class="badge bg-info">{{ ucfirst($history->type) }}</span>
                            @else
                                <span class="badge bg-secondary">{{ ucfirst($history->type) }}</span>
                            @endif
                        </td>
                        <td class="text-center"><b>{{ 'Rp ' . number_format($history->amount, 2, ',', '.') }}</b></td>
                        <td class="text-center">
                            @if($history->type === 'top-up')
                                <span class="text-success"><b>+ {{ 'Rp ' . number_format($history->amount, 2, ',', '.') }}</b></span>
                            @elseif($history->type === 'transaction')
                                <span class="text-danger"><b>- {{ 'Rp ' . number_format($history->amount, 2, ',', '.') }}</b></span>
                            @else
                                <b>{{ 'Rp ' . number_format($history->amount, 2, ',', '.') }}</b>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('js')
    <script>
        $(document).ready(function() {
            var table = $('#orders-table').DataTable();

            $('#type_filter').on('change', function() {
                var type = $(this).val();
                table.columns(2).search(type).draw();
            });
        });
    </script>
@stop
