@extends('adminlte::page')

@section('title', 'Create Order')

@section('content_header')
    <h1>Create Order</h1>
@stop

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Order Form</h3>
        </div>
        <div class="card-body">
            <form id="order-form" action="{{ route('user.orders.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="type">Type</label>
                    <select name="type" id="type" class="form-control @error('type') is-invalid @enderror" required>
                        <option value="">Select Type</option>
                        <option value="top-up" {{ old('type') == 'top-up' ? 'selected' : '' }}>Top-Up</option>
                        <option value="transaction" {{ old('type') == 'transaction' ? 'selected' : '' }}>Transaction</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="amount">Amount</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Rp</span>
                        </div>
                        <input type="text" name="amount" id="amount" class="form-control @error('amount') is-invalid @enderror" value="{{ old('amount') }}" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea name="description" id="description" class="form-control @error('description') is-invalid @enderror">{{ old('description') }}</textarea>
                </div>
                <div class="form-group" id="receipt-group" style="display: none;">
                    <label for="receipt">Receipt</label>
                    <input type="file" name="receipt" id="receipt" class="form-control @error('receipt') is-invalid @enderror">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>

    <script>

        function formatNumber(value) {
            return value.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        }

        function getNumber(value) {
            return value.replace(/,/g, '');
        }

        document.getElementById('type').addEventListener('change', function () {
            if (this.value === 'top-up') {
                document.getElementById('receipt-group').style.display = 'block';
            } else {
                document.getElementById('receipt-group').style.display = 'none';
            }
        });

        document.addEventListener('DOMContentLoaded', function () {
            if (document.getElementById('type').value === 'top-up') {
                document.getElementById('receipt-group').style.display = 'block';
            }

            const amountInput = document.getElementById('amount');
            amountInput.addEventListener('input', function () {
                const value = getNumber(this.value);
                this.value = formatNumber(value);
            });

            document.getElementById('order-form').addEventListener('submit', function () {
                const amountInput = document.getElementById('amount');
                amountInput.value = getNumber(amountInput.value);
            });
        });
    </script>
@stop
