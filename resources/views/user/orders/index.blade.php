@extends('adminlte::page')

@section('title', 'My Orders')

@section('plugins.Datatables', true)

@section('content_header')
    <h1>My Orders</h1>
@stop

@section('content')
    <div class="col-md-2 pb-4">
        <a href="{{ route('user.orders.create') }}" class="btn btn-block btn-primary">Add Order</a>
    </div>
    <div class="row">
        <div class="col-md-3 mb-3">
            <label for="status_filter" class="form-label">Filter by Status:</label>
            <select id="status_filter" class="form-control select2">
                <option value="">All Statuses</option>
                <option value="pending">Pending</option>
                <option value="completed">Completed</option>
                <option value="reject">Rejected</option>
            </select>
        </div>
    </div>
    <div class="card mt-3">
        <div class="card-body">
            <table id="orders-table" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Order Number</th>
                    <th>Type</th>
                    <th>Amount</th>
                    <th>Description</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">View Receipt</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $order)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $order->order_number }}</td>
                        <td>{{ ucfirst($order->type) }}</td>
                        <td>{{ 'Rp ' . number_format($order->amount, 2, ',', '.') }}</td>
                        <td>{{ $order->description }}</td>
                        <td class="text-center">
                            @if($order->status === 'pending')
                                <span class="badge bg-warning">{{ ucfirst($order->status) }}</span>
                            @elseif($order->status === 'completed')
                                <span class="badge bg-success">{{ ucfirst($order->status) }}</span>
                            @elseif($order->status === 'reject')
                                <span class="badge bg-danger">{{ ucfirst($order->status) }}</span>
                            @else
                                <span class="badge bg-secondary">{{ ucfirst($order->status) }}</span>
                            @endif
                        </td>
                        <td class="text-center">
                            @if($order->receipt)
                                <button class="btn btn-primary" data-toggle="modal" data-target="#receiptModal{{ $order->id }}">View Receipt</button>
                                <!-- Modal -->
                                <div class="modal fade" id="receiptModal{{ $order->id }}" tabindex="-1" role="dialog" aria-labelledby="receiptModalLabel{{ $order->id }}" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="receiptModalLabel{{ $order->id }}">Receipt</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <img src="{{ asset('storage/' . $order->receipt) }}" class="img-fluid" alt="Receipt">
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @else
                                -
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('js')
    <script>
        $(document).ready(function() {
            var table = $('#orders-table').DataTable();
            var orderId;

            $('#status_filter').on('change', function() {
                var status = $(this).val();
                table.columns(5).search(status).draw();
            });
        });
    </script>
@stop
