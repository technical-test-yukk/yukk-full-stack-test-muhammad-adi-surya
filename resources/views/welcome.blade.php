<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Welcome</title>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;600&display=swap" rel="stylesheet">
    <style>
        body {
            font-family: 'Roboto', sans-serif;
            margin: 0;
            padding: 0;
            background-color: #F3F4F6;
        }
        .container {
            max-width: 1200px;
            margin: 0 auto;
            padding: 20px;
            text-align: center;
        }
        .logo {
            width: 150px;
            height: 150px;
            margin: 0 auto;
        }
        .btn {
            display: inline-block;
            padding: 10px 20px;
            margin-top: 20px;
            background-color: #EF4444;
            color: #FFF;
            text-decoration: none;
            border-radius: 5px;
            transition: background-color 0.3s ease;
        }
        .btn:hover {
            background-color: #DC2626;
        }
    </style>
</head>
<body>
<div class="container">
    <h1>Welcome</h1>
    <p>Technical Test PT YUKK Kreasi Indonesia.</p>
    @if (Route::has('login'))
        @auth
            <a href="{{ url('/home') }}" class="btn">Dashboard</a>
        @else
            <a href="{{ route('login') }}" class="btn">Log in</a>
            @if (Route::has('register'))
                <a href="{{ route('register') }}" class="btn">Register</a>
            @endif
        @endauth
    @endif
</div>
</body>
</html>
