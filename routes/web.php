<?php

use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Auth::routes();

Route::middleware(['auth', 'admin'])->prefix('admin')->name('admin.')->group(function () {
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
    Route::resource('users', UserController::class);
    Route::post('/orders/confirm/{order}', [OrderController::class, 'confirm'])->name('user.orders.confirm');
    Route::post('/orders/reject/{order}', [OrderController::class, 'reject'])->name('user.orders.reject');
    Route::get('/orders', [OrderController::class, 'index'])->name('orders');
});

Route::middleware(['auth', 'user'])->prefix('user')->name('user.')->group(function () {
    Route::get('/dashboard', [App\Http\Controllers\User\DashboardController::class, 'index'])->name('dashboard');
    Route::resource('orders', App\Http\Controllers\User\OrderController::class)->only(['index', 'create', 'store']);
    Route::resource('order-history', App\Http\Controllers\User\OrderController::class)->only(['history']);
    Route::get('order-history', [App\Http\Controllers\User\OrderController::class, 'history'])->name('order-history');
});
