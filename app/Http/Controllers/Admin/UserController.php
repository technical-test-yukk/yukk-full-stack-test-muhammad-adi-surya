<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = User::all();
            return DataTables::of($data)
                ->addColumn('actions', function($row) {
                    $editUrl = route('admin.users.edit', $row->id);
                    $deleteUrl = route('admin.users.destroy', $row->id);
                    return '
                    <button data-id="' . $row->id . '" data-name="' . $row->name . '" data-email="' . $row->email . '" data-role="' . $row->role . '" class="btn btn-info viewBtn" data-toggle="modal" data-target="#viewUserModal">View</button>
                    <button data-id="' . $row->id . '" data-name="' . $row->name . '" data-email="' . $row->email . '" data-role="' . $row->role . '" class="btn btn-primary editBtn" data-toggle="modal" data-target="#editUserModal">Edit</button>
                    <button data-id="' . $row->id . '" class="btn btn-danger deleteBtn" data-toggle="modal" data-target="#deleteUserModal">Delete</button>
                ';
                })
                ->rawColumns(['actions'])
                ->make(true);
        }

        return view('admin.users.index');
    }

    public function create()
    {
        return view('admin.users.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
            'role' => 'required|string|in:admin,user',
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'role' => $request->role,
        ]);

        if ($request->ajax()) {
            return response()->json(['success' => 'User created successfully.', 'user' => $user]);
        }

        return redirect()->route('admin.users.index')->with('success', 'User created successfully.');
    }

    public function show(User $user)
    {
        return view('admin.users.show', compact('user'));
    }

    public function edit(User $user)
    {
        return view('admin.users.edit', compact('user'));
    }

    public function update(Request $request, User $user)
    {
        $validatedData = $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,' . $user->id,
            'role' => 'required|string|in:admin,user',
        ]);

        $user->update($validatedData);

        if ($request->ajax()) {
            return response()->json(['success' => 'User updated successfully.', 'user' => $user]);
        }

        return redirect()->route('admin.users.index')->with('success', 'User updated successfully.');
    }



    public function destroy(Request $request, User $user)
    {
        $user->delete();

        if ($request->ajax()) {
            return response()->json(['success' => 'User deleted successfully.']);
        }

        return redirect()->route('admin.users.index')->with('success', 'User deleted successfully.');
    }
}

