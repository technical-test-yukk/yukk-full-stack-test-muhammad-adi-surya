<?php

namespace App\Http\Controllers\Admin;

use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public function index()
    {
        $orders = Order::orderBy('id', 'desc')->get();
        return view('admin.orders.index', compact('orders'));
    }

    public function show(Order $order)
    {
        return view('admin.orders.show', compact('order'));
    }

    public function edit(Order $order)
    {
        return view('admin.orders.edit', compact('order'));
    }

    public function update(Request $request, Order $order)
    {
        $request->validate([
            'status' => 'required|string|in:pending,confirmed',
        ]);

        $order->update($request->all());

        return redirect()->route('admin.orders.index')->with('success', 'Order updated successfully.');
    }

    public function destroy(Order $order)
    {
        $order->delete();

        return redirect()->route('admin.orders.index')->with('success', 'Order deleted successfully.');
    }

    public function confirm(Order $order)
    {
        try {
            $user = User::find($order->user_id);
            $current_balance = $user->current_balance ?? 0;

            if($order->type == 'top-up'){
                $balance = $current_balance + $order->amount;
            } elseif ($order->type == 'transaction'){
                $balance = $current_balance - $order->amount;
            } else {
                $balance = $current_balance;
            }

            $order->status = 'completed';
            $order->save();

            $user->update([
                'current_balance' =>  $balance,
            ]);

            return response()->json(['success' => 'Order confirmed successfully.']);
        } catch (\Exception $e) {
            return response()->json(['error' => 'An error occurred while confirming the order.']);
        }
    }

    public function reject(Order $order)
    {
        $order->status = 'reject';
        $order->save();

        return response()->json(['success' => 'Order reject successfully.']);
    }


}

