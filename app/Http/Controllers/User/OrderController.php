<?php

namespace App\Http\Controllers\User;

use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public function index()
    {
        $orders = auth()->user()->orders()->orderBy('id', 'desc')->get();
        return view('user.orders.index', compact('orders'));
    }

    public function create()
    {
        return view('user.orders.create');
    }

    public function store(Request $request)
    {
        $user = auth()->user();
        $request->validate([
            'type' => 'required|string|in:top-up,transaction',
            'amount' => [
                'required',
                'numeric',
                'regex:/^\d{1,12}(\.\d{1,2})?$/',
                function ($attribute, $value, $fail) use ($user, $request) {
                    if ($request->type === 'transaction' && $value > $user->current_balance) {
                        $fail('Balance Tidak Mencukupi, balance anda sekarang :'.'Rp ' . number_format($user->current_balance, 0, ',', '.'));
                    }
                },
            ],
            'description' => 'nullable|string',
            'receipt' => 'required_if:type,top-up|file|mimes:jpg,jpeg,png',
        ]);

        $order = new Order();
        $order->user_id = auth()->id();
        $order->order_number = $this->generateOrderNumber();
        $order->type = $request->type;
        $order->amount = $request->amount;
        $order->description = $request->description;
        if ($request->hasFile('receipt')) {
            $order->receipt = $request->file('receipt')->store('receipts', 'public');
        }
        $order->save();

        return redirect()->route('user.orders.index')->with('success', 'Order created successfully.');
    }

    private function generateOrderNumber()
    {
        $timestamp = now()->format('YmdHis');
        $randomNumber = str_pad(random_int(0, 9999), 4, '0', STR_PAD_LEFT);

        return 'ORD-' . $timestamp . '-' . $randomNumber;
    }

    public function history()
    {
        $user = auth()->user();
        $histories = $user->histories()->orderBy('id', 'desc')->paginate(10);

        $topup_total = $user->histories()->where('type', 'top-up')->sum('amount');
        $transaction_total = $user->histories()->where('type', 'transaction')->sum('amount');

        $current_balance = $user->current_balance;

        return view('user.orders.history', compact('histories', 'current_balance', 'topup_total', 'transaction_total'));
    }
}
