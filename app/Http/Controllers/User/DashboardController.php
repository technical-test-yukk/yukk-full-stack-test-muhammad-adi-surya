<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $user = auth()->user();

        $user->load(['orders']);

        $orders = $user->orders;

        $orderCount = $orders->count();

        $pendingCount = $orders->where('status', 'pending')->count();
        $completedCount = $orders->where('status', 'completed')->count();
        $rejectCount = $orders->where('status', 'reject')->count();

        $latestOrders = $orders->sortByDesc('created_at')->take(10);

        return view('user.dashboard', compact('latestOrders', 'orderCount', 'completedCount', 'pendingCount', 'rejectCount'));
    }


}
