composer install

php artisan db:wipe --force

php artisan key:generate
php artisan config:clear
php artisan route:clear
php artisan event:clear
php artisan view:clear

php artisan migrate --force

php artisan db:seed
