# Proyek Laravel Sederhana untuk keperluan Technical Test di PT YUKK Kreasi Indonesia

## Deskripsi

Proyek ini adalah aplikasi web berbasis Laravel yang menyediakan fitur login dan registrasi, manajemen pengguna dengan dua role (admin dan user), dan manajemen order. Admin dapat mengelola pengguna dan memverifikasi order, sementara pengguna dapat melihat dan membuat order. Aplikasi ini menggunakan template AdminLTE untuk tampilan antarmuka.

## Fitur 

1. **Autentikasi**: Login dan register
2. **Role Management**: Admin dan User
3. **Template**: Menggunakan AdminLTE
4. **Middleware**: Untuk membatasi akses berdasarkan role
5. **Dashboard**: Terpisah untuk admin dan user
6. **Manajemen Pengguna**: Admin dapat mengelola pengguna
7. **Manajemen Order**: Admin dapat mengonfirmasi order, dan user dapat membuat serta melihat order
8. **Order Management**: Dua tipe order (top-up dan transaksi)
9. **Riwayat Order**: Pagination, search, dan filter

## Requirment

- PHP >= 7.4
- Composer
- Node.js & NPM
- Database (MySQL)


## Instalasi

Ikuti langkah-langkah berikut untuk menginstal dan menjalankan proyek ini:

1. **Clone repository ini**:
    ```bash
    git clone https://gitlab.com/technical-test-yukk/yukk-full-stack-test-muhammad-adi-surya.git
    cd repository
    ```

2. **Buat file `.env` dari contoh**:
    ```bash
    cp .env.example .env
    ```

3. **Konfigurasi file `.env`**:
    - Atur koneksi database:
      ```env
      DB_CONNECTION=mysql
      DB_HOST=127.0.0.1
      DB_PORT=3306
      DB_DATABASE=nama_database
      DB_USERNAME=username_database
      DB_PASSWORD=password_database
      ```

4. **Jalankan skrip dev.sh untuk instalasi dan migrasi**:
    ```bash
    sh dev.sh
    ```

    Skrip `dev.sh` berisi perintah berikut:
    ```bash
    #!/bin/bash

    composer install

    php artisan db:wipe --force

    php artisan key:generate
    php artisan config:clear
    php artisan route:clear
    php artisan event:clear
    php artisan view:clear

    php artisan migrate --force

    php artisan db:seed
    ```

5. **Instal dependensi NPM**:
    ```bash
    npm install
    npm run dev
    ```

## Menjalankan Aplikasi

1. **Jalankan server lokal**:
    ```bash
    php artisan serve
    ```

2. **Akses aplikasi di browser**:
    ```
    http://127.0.0.1:8000
    ```

## Akun Uji Coba

Untuk Demo silahkan klik link :
<a href="https://yukk.adisurya.online" target="_blank">DEMO</a>


Gunakan akun berikut untuk login:

### Admin
- **Email**: admin@example.com
- **Password**: password

### User
- **Email**: user@example.com
- **Password**: password

## Struktur Proyek

- `app/Http/Controllers/`: Berisi controller untuk menangani logika aplikasi
- `app/Models/`: Berisi model untuk interaksi dengan database
- `database/migrations/`: Berisi file migrasi untuk membuat tabel database
- `database/seeders/`: Berisi file seeder untuk mengisi data awal
- `resources/views/`: Berisi template blade untuk tampilan

## Rincian Fitur

### Autentikasi
- Menggunakan Laravel Jetstream dengan Livewire
- Login dan register

### Role Management
- Menggunakan package Spatie Laravel-Permission
- Admin dan User role

### Template AdminLTE
- Template AdminLTE diintegrasikan di layout aplikasi

### Middleware
- Middleware untuk membatasi akses berdasarkan role

### Manajemen Pengguna (Admin)
- CRUD pengguna
- Hanya admin yang dapat mengakses

### Manajemen Order
- Admin dapat mengonfirmasi order
- User dapat membuat order dengan tipe top-up dan transaksi
- Validasi form order

### Riwayat Order
- Pagination
- Search
- Filter

## Pengujian

Pastikan untuk melakukan pengujian fitur-fitur berikut:

1. **Login dan register**:
    - Coba login dengan akun admin dan user yang sudah disediakan
    - Coba register akun baru

2. **Role Management**:
    - Admin dapat mengakses dashboard admin
    - User dapat mengakses dashboard user

3. **Manajemen Pengguna**:
    - Admin dapat menambah, mengedit, dan menghapus pengguna

4. **Manajemen Order**:
    - User dapat membuat order dengan tipe top-up dan transaksi
    - Admin dapat mengonfirmasi order serta menolak order dan mengubah saldo user

5. **Riwayat Order**:
    - Pagination, search, dan filter berfungsi dengan baik

## Reviewer Catatan

Proyek ini dikembangkan untuk memenuhi kebutuhan technical test di PT YUKK Kreasi Indonesia. Silakan lakukan review terhadap kode dan fitur yang telah diimplementasikan. Jika ada pertanyaan atau feedback, jangan ragu untuk menghubungi saya.

Terima kasih.



##

<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

You may also try the [Laravel Bootcamp](https://bootcamp.laravel.com), where you will be guided through building a modern Laravel application from scratch.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains thousands of video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for funding Laravel development. If you are interested in becoming a sponsor, please visit the [Laravel Partners program](https://partners.laravel.com).

### Premium Partners

- **[Vehikl](https://vehikl.com/)**
- **[Tighten Co.](https://tighten.co)**
- **[WebReinvent](https://webreinvent.com/)**
- **[Kirschbaum Development Group](https://kirschbaumdevelopment.com)**
- **[64 Robots](https://64robots.com)**
- **[Curotec](https://www.curotec.com/services/technologies/laravel/)**
- **[Cyber-Duck](https://cyber-duck.co.uk)**
- **[DevSquad](https://devsquad.com/hire-laravel-developers)**
- **[Jump24](https://jump24.co.uk)**
- **[Redberry](https://redberry.international/laravel/)**
- **[Active Logic](https://activelogic.com)**
- **[byte5](https://byte5.de)**
- **[OP.GG](https://op.gg)**

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Code of Conduct

In order to ensure that the Laravel community is welcoming to all, please review and abide by the [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
